from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Sign(MethodView):
    def get(self):
        generic_name = request.args.get('generic_name')
        header = f"{generic_name}"
        return render_template('sign.html', header=header, generic_name=generic_name)

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        generic_name = request.args.get('generic_name')
        model = gbmodel.get_model()
        model.insert(generic_name, request.form['title'], request.form['rating'], request.form['illness'], request.form['body'])
        return redirect(url_for('index', generic_name=generic_name))


