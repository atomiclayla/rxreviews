from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel
import csv

filename = 'data.csv'
class List(MethodView):
    def get(self):
        generic_names = set()

        # Open the CSV file
        with open(filename, 'r') as file:
            # Create a CSV reader object
            reader = csv.DictReader(file)

            # Iterate over each row in the CSV file
            for row in reader:
                # Append the 'genericName' field from the current row to the list
                if row['drug_name_generic'].strip() is not None:
                    generic_names.add(row['drug_name_generic'].lstrip("*NF* "))

        sorted_names = sorted(generic_names)

        return render_template('list.html', generic_names=sorted_names)
