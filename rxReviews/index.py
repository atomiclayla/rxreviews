from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Index(MethodView):
    def get(self):
        generic_name = request.args.get('generic_name')
        header = f"{generic_name}"
        model = gbmodel.get_model()
        entries = [dict(prescription=row[0], title=row[1], rating=row[2], illness=row[3], body=row[4] ) for row in model.select() if row[0] == generic_name]
        avg_rating = 0.0
        if entries:
            total_rating = sum(float(entry['rating']) for entry in entries)
            avg_rating = total_rating / len(entries)

        return render_template('index.html',entries=entries, header=header, rating=avg_rating)
