"""
A simple guestbook flask app.
"""
import flask, os
from flask import redirect, request, url_for, render_template
from flask.views import MethodView
from list import List
from index import Index
from sign import Sign

app = flask.Flask(__name__)       # our Flask app

app.add_url_rule('/',
                 view_func=List.as_view('list'),
                 methods=["GET"])

app.add_url_rule('/sign/',
                 view_func=Sign.as_view('sign'),
                 methods=['GET', 'POST'])

app.add_url_rule('/index/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=int(os.environ.get('PORT',5000)))
