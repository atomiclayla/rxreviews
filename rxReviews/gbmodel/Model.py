class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, prescription, title, rating, illness, body):
        """
        Inserts entry into database
        :param prescription: String
        :param title: String
        :param rating: Float
        :param illness: String
        :param body: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
