"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.reviewlist = []

    def select(self):
        """
        Returns reviewlist list of lists
        Each list in reviewlist contains: prescription, title, rating, illness, body
        :return: List of lists
        """
        return self.reviewlist

    def insert(self, prescription, title, rating, illness, body):
        """
        Appends a new list of values representing new message into studyentries
        :param buildingName: String
        :param buildingCode: String
        :param buildingFloor: String
        :param roomNumber: String
        :param rating: String
        :return: True
        """
        params = [prescription, title, rating, illness, body]
        self.reviewlist.append(params)
        return True
