"""
A simple guestbook flask app.
Data is stored in a SQLite database that looks something like the following:

+------------+------------------+------------+----------------+
| Name       | Email            | signed_on  | message        |
+============+==================+============+----------------+
| John Doe   | jdoe@example.com | 2012-05-28 | Hello world    |
+------------+------------------+------------+----------------+

This can be created with the following SQL (see bottom of this file):

    create table guestbook (name text, email text, signed_on date, message);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from reviews")
        except sqlite3.OperationalError:
            cursor.execute("create table reviews(prescription text, title text, rating float, illness text, body text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, email, date, message
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM reviews")
        return cursor.fetchall()

    def insert(self, prescription, title, rating, illness, body):
        """
        Inserts entry into database
        :param buildingName: String
        :param buildingCode: String
        :param buildingFloor: String
        :param roomNumber: String
        :param rating: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'prescription':prescription, 'title':title, 'rating':rating, 'illness':illness, 'body':body}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into reviews (prescription, title, rating, illness, body) VALUES (:prescription, :title, :rating, :illness, :body)", params)
        connection.commit()
        cursor.close()
        return True
